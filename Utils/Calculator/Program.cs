﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // method get 5 (1)
            int a = GetValue5();

            // method get 6 (2)
            int b = GetValue6();

            // Method get sum (1) + (2)
            int sum = GetSum(a, b);

            Console.WriteLine(sum);
            Console.ReadKey();
        }

        static int GetValue5()
        {
            return 5;
        }

        static int GetValue6()
        {
            return 6;
        }

        // sum
        static int GetSum(int a, int b)
        {
            int sum = a + b;
            return sum;
        }
    }
}
